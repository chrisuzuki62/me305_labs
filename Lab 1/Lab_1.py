''' @file           Lab_1.py
    @brief          Produce patterns of light using a finite state machine
    @details        The file produces three different light patterns of the following
                    1. Square Wave
                    2. Sine Wave
                    3. Sawtooth Wave
                    
                    This program runs on a Nucleo Board and is a finite state machine 
                    that cycles through various LED pulse patterns. To cycle through 
                    the patterns the user must click the blue user button on the board. 
                    \image html Lab1.jpg "State Transition Diagram"
                    
                    If the program runs successfully it should look like the video below
                    
                    \htmlonly
                   <iframe width="560" height="315" src="https://www.youtube.com/embed/6KJv1jHqP38" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    \endhtmlonly
                    
                   
    @author         Chris Suzuki
    @author         Cade Liberty
    @date           October 6, 2021
'''

import math
import utime
import pyb

## @brief   Assigns the A5 pin to a callable variable pinA5
#  @details This  takes in the pyb board and assigns the pin A5 on the board
#           to a variable called pinA5 where it can be called later in the code
pinA5 = pyb.Pin (pyb.Pin.cpu.A5) 
  
## @brief   Assigns the C13 pin to a callable variable pinC13
#  @details This takes in the pyb board and assigns the pin C13 on the board
#           to a variable called pinC13 where it can be called later in the code
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

## @brief   Initlizes a variable representing the button to be false
#  @details This takes creates a variable to represent the button and assigns it
#           a false value
button = False

## @brief   Assigns a variable to call timer 2 on the board
#  @details This uses the timer two on the board and assigns it to a callable
#           variable which can be used later
tim2 = pyb.Timer(2, freq = 20000)

## @brief   Assigns the first channel of the timer to a callable variable
#  @details Takes in the first channel available on timer 2 and assigns it to a 
#           variable that can be called later
t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)


# Function Definitions
def SQR_wave(current_time):
    '''@brief       Cause an LED to blink in an on and off or square wave fashion
       @details     This function produces a square wave function when looking at
                    an LED. The LED will blink on causing the value to rise to 1
                    and then blink off having it fall back to 0. To produce this 
                    blinking the functions runs through a logical statement returning
                    100 when it is true and 0 when it is false. The wave repeats
                    every second performing this pattern until a button is pressed
                    moving on to the next wave function.
       @param       Takes in a current time which is the difference between the start
                    time when the function is called and the end time which updates
                    everytime the pattern is ran
       @return      Returns either a 100 or 0 based on the remainder that is found
                    from using the mod function

    '''
    return 100*(current_time % 1.0 < 0.5)


def SIN_wave(current_time):
    '''@brief       Cause an LED to blink sinusoidal wave fashion
       @details     This function produces a sinusoidal wave using by varying the 
                    brightness of the LED. To produce this blinking the functions 
                    uses the mathematical function sine and which varies the value
                    between -1 and 1. From there we shift the wave up and reduce
                    the amplitude to produce a sine wave with the output of the LED.
                    The wave repeats every second performing this pattern until a 
                    button is pressed moving on to the next wave function.
       @param       Takes in a current time which is the difference between the start
                    time when the function is called and the end time which updates
                    everytime the pattern is ran
       @return      Returns either a sinusoidal wave based on the math function sine

    '''
    return 50 + 50*(math.sin(current_time))


def SWT_wave(Current_time):
    '''@brief       Cause an LED to blink in an on and off or sawtooth wave fashion
       @details     This function produces a swooth wave function by increasing the
                    brightness until full brightness and then turning off momentarily.
                    To produce this blinking the function uses the mod function to
                    linearly increase the brightness until it reaches an .
                    The wave repeats every second performing this pattern until a 
                    button is pressed moving on to the next wave function.
       @param       Takes in a current time which is the difference between the start
                    time when the function is called and the end time which updates
                    everytime the pattern is ran
       @return      Returns an increasing value from the mod function that resets
                    every second to produce a sawtooth wave
    '''
    return 100*(Current_time % 1.0)


def onButtonPressFCN(IRQ_src):
    '''@brief       Sets the button variable to True
       @details     This function is used as a callback for the interuppt to change states and takes in a 
                    button press to create a global variable button that is set to true 
                    whenever the button has been pushed. 
       @param       Takes in IRQ_src as the interrupt to state whether a button has been pushed or not.

    '''
    
    global button
    button = True   
    

#Running the code
if __name__ == '__main__':
    ## @brief   Sets the state of the finite state machine
    #  @details Uses the states as defined in the finite state machine and sets
    #           it to the correct state which changes what happens with the LED.
    #           The states can go from 0-4
    state = 0
    
    ## @brief   Initializes a callback for the button
    #  @details This uses the callback feature in Python to create a callback that
    #           enables the use of the button to change the state of the state machine
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    print('Welcome to the Nucleo Hardware! To cycle the three LED patterns push the button labeled B1.')
   
    state = 1
    
    while (True):
        try:
            # Define an waiting for button push state
            if (state == 1):
                if button == True:
                    state = 2
                    # run the state 2 code
                    print('Square Wave Executing')
                    print('For the Sine Wave pattern push the button again.')
                    button = False
                    
                    ## @brief   Sets the start time of the Square wave
                    #  @details This uses the utime module to count the time in
                    #           which the Square wave form takes place and then sets it
                    #           as a variable
                    SQR_Start = utime.ticks_ms()
           
            # Square wave state (state 2)
            elif (state == 2):
                
                ## @brief   Sets the stop time of the Square wave
                #  @details This uses the utime module to count the time in
                #           which the Square wave form takes place and then sets it
                #           as a variable. This variable is updated everytime the 
                #           loop is ran.
                SQR_Stop = utime.ticks_ms()
                
                ## @brief   Compute the duration that the Square wave has persisted
                #  @details This uses the utime module and functionality in it 
                #           to determine the difference between the start and stop 
                #           times as a variable. This variable is updated everytime 
                #           the loop is ran
                SQR_dur = utime.ticks_diff(SQR_Stop,SQR_Start)/1000
                
                ## @brief   Sets the value of the Square wave
                #  @details This variable uses the duration of the square wave above
                #           and runs it through the Square wave function above to
                #           produce a value between 0 and 100
                SQR_value = SQR_wave(SQR_dur)
                
                ## @breif   Sets the width and brightness of the LED
                #  @details This variable takes in the value produced above and 
                #           sets the width and brightness of the LED causing it to
                #           change the brightness every pass through the loop.
                t2ch1.pulse_width_percent(SQR_value)
                if button == True:
                    state = 3
                    print('Sine Wave executing')
                    print('For the Sawtooth Wave pattern push the button again.')
                    button = False
                    
                    ## @brief   Sets the start time of the Sine wave
                    #  @details This uses the utime module to count the time in
                    #           which the Sine wave form takes place and then sets it
                    #           as a variable
                    SIN_Start = utime.ticks_ms()
                    
            # Sine wave state (state 3)        
            elif (state==3):
                
                ## @brief   Sets the stop time of the Sine wave
                #  @details This uses the utime module to count the time in
                #           which the Sine wave form takes place and then sets it
                #           as a variable. This variable is updated everytime the 
                #           loop is ran.
                SIN_Stop = utime.ticks_ms()
                
                ## @brief   Compute the duration that the Sine wave has persisted
                #  @details This uses the utime module and functionality in it 
                #           to determine the difference between the start and stop 
                #           times as a variable. This variable is updated everytime 
                #           the loop is ran
                SIN_dur = utime.ticks_diff(SIN_Stop,SIN_Start)*(2*math.pi)/10000
                
                ## @brief   Sets the value of the Sine wave
                #  @details This variable uses the duration of the sine wave above
                #           and runs it through the Sine wave function above to
                #           produce a value between 0 and 100
                SIN_value = SIN_wave(SIN_dur)
                
                ## @breif   Sets the width and brightness of the LED
                #  @details This variable takes in the value produced above and 
                #           sets the width and brightness of the LED causing it to
                #           change the brightness every pass through the loop.
                t2ch1.pulse_width_percent(SIN_value)
                if button == True:
                    state = 4
                    print('Sawtooth Wave executing')
                    print('For the Square Wave pattern push the button again.')
                    button = False
                    
                    ## @brief   Sets the start time of the Sawtooth wave
                    #  @details This uses the utime module to count the time in
                    #           which the Sawtooth wave form takes place and then sets it
                    #           as a variable
                    SWT_Start = utime.ticks_ms()
                    #run the state 4 code
            
            # Sawtooth wave state (state 4)
            elif (state==4):
               
                ## @brief   Sets the stop time of the Sawtooth wave
                #  @details This uses the utime module to count the time in
                #           which the Sawtooth wave form takes place and then sets it
                #           as a variable. This variable is updated everytime the 
                #           loop is ran.
                SWT_Stop = utime.ticks_ms()
               
                ## @brief   Compute the duration that the Sawtooth wave has persisted
                #  @details This uses the utime module and functionality in it 
                #           to determine the difference between the start and stop 
                #           times as a variable. This variable is updated everytime 
                #           the loop is ran
                SWT_dur = utime.ticks_diff(SWT_Stop,SWT_Start)/1000
                
                ## @brief   Sets the value of the Sawtooth wave
                #  @details This variable uses the duration of the sawtooth wave above
                #           and runs it through the Sawtooth wave function above to
                #           produce a value between 0 and 100
                SWT_value = SWT_wave(SWT_dur)
                
                ## @breif   Sets the width and brightness of the LED
                #  @details This variable takes in the value produced above and 
                #           sets the width and brightness of the LED causing it to
                #           change the brightness every pass through the loop.
                t2ch1.pulse_width_percent(SWT_value)
                if button == True:
                    state = 2
                    print('Square Wave executing')
                    print('For the Sine Wave pattern push the button again.')
                    button = False
                    
                    ## @brief   Sets the start time of the Square wave
                    #  @details This uses the utime module to count the time in
                    #           which the Square wave form takes place and then sets it
                    #           as a variable
                    SQR_Start = utime.ticks_ms()

        # terminating program        
        except KeyboardInterrupt:
            break
    print('Program Terminating')