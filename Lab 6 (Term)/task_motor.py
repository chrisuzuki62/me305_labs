""" @file task_motor.py
    @brief A task used to retrieve data from an encoder and set the encoder reading to 0
    @details This file creates a task that is used to operate an motor and provide
             its data in a usable shared format. The class has 2 methods in it. One
             method is used to instatntiate the task encoder objects and the other
             method is used to run the motor, set the duty cycle of the motor and
             clear when directed by the user interface.
             Below is a diagram showing how data is retrieved and shared
             \image html IMG_8179.jpg "Task and Shares Diagram"
             Below is data showing 2 runs at different duty cycles and the data collected 
             using the 'g' function.
             \image html Lab3_1.JPG "Postion Speed of Motor at 50% Duty Cycle"
             \image html Lab3_2.JPG "Postion Speed of Motor at 100% Duty Cycle"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
"""
import DRV8847, pyb, utime

class task_motor:
    ''' @brief Encoder task for Lab 0x02
        @details Instantiates several motor objects that are used to run the connected
                 motors. The class has two methods, one used to create the motor object
                 and one to run the motors. Inside the run method any fault flags can
                 be cleared
    '''
    def __init__(self, motor1_share, motor2_share,fault_share):
        ''' @brief Constructs an Motor Task
            @details Creates several variables that are used to run and set the duty of
                     the motors. This statement also turns on the motors to allow them
                     to be run.
            @param motor1_share inputs a share that links the data from this file
                   to the user task so they can communicate
            @param motor2_share inputs a share that links the data from this file
                   to the user task so they can communicate
            @param fault_share inputs a share that links the data from this file
                   to the user task so they can communicate and clear any faults that
                   occur
        '''
        ## Creates a Driver object to run the motors
        self.motor_drv     = DRV8847.DRV8847()      
        ## Creates a motor object to one of the motors to set and run it
        self.motor_1       = self.motor_drv.motor(1, pyb.Pin.cpu.B4, 2, pyb.Pin.cpu.B5)      
        ## Creates a motor object to one of the motors to set and run it
        self.motor_2       = self.motor_drv.motor(3, pyb.Pin.cpu.B0, 4, pyb.Pin.cpu.B1)
        self.motor_drv.enable()  
        ## Creates a share to be able to send data to and from the user interface
        self.motor1_share = motor1_share
        ## Creates a share to be able to send data to and from the user interface
        self.motor2_share = motor2_share
        ##Creates a fault share that enables the user to disable any fault flags
        self.fault_flag = fault_share
        ## sets the data collection period or frequency
        self.period = 5
         
        ## sets when the next time data will be collected
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
    def run(self):
        ''' @brief Continuously runs and when asked changes the duty cycle of the motors
            @details This method runs the motors and sets the PWM duty when called for
                     by the user task. It also clears any fault flags when directed by
                     the user interface
        '''
        ## creates a current time that updates everytime the code is ran. This current
        # time sets the rate at which this program will run
        current_time = utime.ticks_ms()
                 
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            self.motor_1.set_duty(self.motor1_share.read())
            self.motor_2.set_duty(self.motor2_share.read())
            
            if self.fault_flag.read() == True:
                self.motor1_share.write(0)
                self.motor2_share.write(0)
                self.motor_drv.enable()
                self.fault_flag.write(False)
                
            self.next_time = utime.ticks_add(self.next_time, self.period)