''' @file           task_panel.py
    @brief A task used to check for read calibrated data from the touch panel
    @details This task when called creates a class object that takes in 2 shares 
             to read data from touch panel driver. It checks to see if there is a 
             calibration file and if not creates a calibration and writes the x, y 
             coordniates data of the touch panel relative to the center of the panel.
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/8/21
    
'''
import Panel_Driver
import utime
import os
from micropython import const

## State 0 of the touch panel task
S0_INIT    = const(0)

## State 1 of the touch panel task
S1_READ    = const(1)

class task_panel():
     ''' @brief A class used to calibrate and update readings from the touch panel
         @details  This class when called will look for calibration data to correct 
                   the ADC reading values from the Panel Driver.
     '''
     def __init__(self, ADC_share, ADC_vel_share):
        ''' @brief Initializes variables for task panel
            @details This method initiates the task panel variables that will read or write 
                     calibration data for the touch panel and write into shared variables the 
                     corrected touch panel data
            @param ADC_share inputs a share that writes the x and y data from the touch panel
            @param ADC_vel_share inputs a share that writes the x velocity and y velocity from 
                   the touch panel
        '''    
    
        ## Initializes a panel driver class object to be used later in the file
        self.Panel = Panel_Driver.Panel_Driver()
        
        ## sets the data collection period in which the file will run
        self.period = 5
         
        ## sets when the next time data will be run
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_INIT
        
        ## sets a variable to record the time when the function is last run
        self.last_time = 0
        
        ## sets a variable to record the x value of the panel when the function is last run
        self.x_val_last = 0
        
        ## sets a variable to record the y value of the panel when the function is last run
        self.y_val_last = 0
        
        ## Initializes a share that shares the the ADC reading from task panel
        self.ADC_share = ADC_share
        ## Initializes a share that shares the the velocity readings from the task panel
        self.ADC_vel_share = ADC_vel_share
        
     def run(self):
         ''' @brief Runs one iteration of the FSM every time it is called
            @details This method runs the task panel FSM 1 time and verifies 
                     calibration or calirbrates or returns
                     the position that is made contact on the touch panel
                     relative to the center of the panel.
         '''
         # Creates a current time that updates everytime the code is ran. This current
         # time sets the rate at which this program will run
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
            filename = "RT_cal_coeffs.txt"
            if self.state == S0_INIT:
                if filename in os.listdir():
                    with open(filename, 'r') as f:
                        print('reading')
                        # Read the first line of the file
                        cal_data_string = f.readline()
                        # Split the line into multiple strings
                        # and then convert each one to a float
                        self.cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                        print(self.cal_values)
                        self.transition_to(S1_READ)
                else:
                    with open(filename, 'w') as f:
                        # Perform manual calibration
                        print('writing')
                        print(self.Panel.calibrate())
                        # Then, write the calibration coefficients to the file
                        # as a string. The example uses an f-string, but you can
                        # use string.format() if you prefer
                        #print((Kxx, Kxy, Kyx, Kyy, Xc, Yc))
                        f.write(self.Panel.calibrate())
                        print('Done')
                        return 1
            if self.state == S1_READ:
                if self.Panel.z_scan() == True:
                    self.x_val = (self.Panel.x_scan()*(self.cal_values[0]) + (self.Panel.y_scan())*(self.cal_values[1]) + self.cal_values[4])
                    self.ADC_share[0].write(self.x_val)
                    self.y_val = (self.Panel.x_scan()*(self.cal_values[2]) + (self.Panel.y_scan())*(self.cal_values[3]) + self.cal_values[5])
                    self.ADC_share[1].write(self.y_val)
                    self.x_vel = (self.x_val-self.x_val_last)/(utime.ticks_diff(current_time,self.last_time))
                    self.ADC_vel_share[0].write(self.x_vel)
                    self.y_vel = (self.y_val-self.y_val_last)/(utime.ticks_diff(current_time,self.last_time))
                    self.ADC_vel_share[1].write(self.y_vel)
                    self.last_time = current_time
                    self.x_val_last = self.x_val
                    self.y_val_last = self.y_val
                else:
                    self.x_val = 0
                    self.ADC_share[0].write(self.x_val)
                    self.y_val = 0
                    self.ADC_share[1].write(self.y_val)
                    self.x_vel = 0
                    self.ADC_vel_share[0].write(self.x_vel)
                    self.y_vel = 0
                    self.ADC_vel_share[1].write(self.y_vel)
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
     def transition_to(self, new_state):
         ''' @brief      Transitions the FSM to a new state
             @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
             @param      new_state The state to transition to.
         '''
         self.state = new_state
            