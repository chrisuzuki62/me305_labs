''' @file  task_collect.py
    @brief A task used to gather and print readings form the touch panel and IMU
    @details This task contains 3 methods that are used to collect and print the
             data from a touch panel and IMU. The first method initalizes all of
             the variables used in this code. The next method uses a 2 state 
             finite state machine to collect the data from the IMU and the ADC and
             then prints it out. the final method is used to transition the finite
             state machine in the system
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/8/21
    @copyright license term
'''
import pyb, utime, array
from micropython import const

## State 0 of the data collection task
S0_collect = const(0)

## State 1 of the data collection task
S1_print   = const(1)

class task_collect():
    '''@brief A class used to collect and print out readings from an IMU and touch panel
       @details  This class when called and turned on with the correct control from task user 
                 will take in the values from the touch panel ADC reading and the IMU readings 
                 and collect them in a array. After collecting for 10 seconds or when the user
                 specifies the task will then print out the values it has gathered.
    '''
    # The ADC share includes two values, one for the x potion listed first and one for the y position
    # The euler angle share includes two values, one for the roll angle which should be placed first and one for the pitch angle which should be second
    # the collect share is a trigger from the main and should be written to true when the user wants to collect data
    def __init__(self, ADC_share, ADC_vel_share, euler_angle_share, ang_vel_share, collect_share):
        '''@brief Initializes variables for task_collect
           @details This method initiates the task collect variables that are used to create a series of 10
                    different arrays that are used to report out the data from the touch panel and IMU.
           @param ADC_share inputs a share that reads the x and y data from the touch panel
           @param ADC_vel_share inputs a share that reads the x velocity and y velocity from task panel
           @param euler_angle_share inputs a share reads the euler pitch and roll angles from the IMU
           @param ang_vel_share inputs a share that share the pitch and roll velocities from the IMU
           @param collect_share inputs a share that specifies when to collect data from the IMU and the panel
        '''
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_collect
        
        ## sets the data collection period
        self.period = 50

        ## sets when the next time data will be collected
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        ## sets a count for the rate at which the file will print
        self.count= 0
        
        ## sets a variable to count the number of runs through the system
        self.runs = 0
        
        ## Initializes a share that shares the the ADC reading from task panel
        self.ADC_share = ADC_share
        ## Initializes a share that shares the the velocity readings from the task panel
        self.ADC_vel_share = ADC_vel_share
        ## Initializes a share that shares the euler angle reading from task IMU
        self.euler_angle_share = euler_angle_share
        ## Initializes a share that specifies when to collect the data
        self.collect_share = collect_share
        ## Initializes a share that shares the the angular pitch and roll velocities
        self.ang_vel_share = ang_vel_share
        
        ## Creates an array of 201 double type values
        self.ADCx_array = array.array('d', range(201))
        ## Creates an array of 201 double type values            
        self.ADCy_array = array.array('d', range(201))
        ## Creates an array of 201 double type values 
        self.x_vel_array = array.array('d', range(201))
        ## Creates an array of 201 double type values 
        self.y_vel_array = array.array('d', range(201))
        ## Creates an array of 201 double type values
        self.time_array = array.array('i', range(201))
        ## Creates an array of 201 double type values
        self.roll_array = array.array('d', range(201))
        ## Creates an array of 201 double type values
        self.pitch_array = array.array('d', range(201))
        ## Creates an array of 201 double type values 
        self.diff_array = array.array('d',range(201))
        ## Creates an array of 201 double type values
        self.roll_vel_array = array.array('d', range(201))
        ## Creates an array of 201 double type values
        self.pitch_vel_array = array.array('d', range(201))
    
    def collect(self):
        '''@brief Collects and prints out the data collected from the touch panel and IMU
           @details This method when called will collect each of the data readings from the 
                    IMU and touch panel for 10 seconds and then prints it out. The order in
                    which it prints is time, x, y, roll, pitch, x velocity, y velocity, roll
                    velocity, and pitch velocity.
        '''
        ## Creates a timer at the current time to determine when the file runs
        current_time = utime.ticks_ms()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0_collect:
                if self.collect_share.read() == True:
                   if self.runs < (10e3/self.period)+1:
                        ## records x position data from the touch panel
                        self.ADCx_array[self.runs] = self.ADC_share[0].read()
                        ## records y position data from the touch panel
                        self.ADCy_array[self.runs] = self.ADC_share[1].read()
                        ## records roll angle data from the IMU
                        self.roll_array[self.runs] = self.euler_angle_share[0].read()
                        ## records pitch angle data from the IMU
                        self.pitch_array[self.runs] = self.euler_angle_share[1].read()
                        ## records time data for the run
                        self.time_array[self.runs] = utime.ticks_ms()
                        ## records the x velocity data of the ball
                        self.x_vel_array[self.runs] = self.ADC_vel_share[0].read()
                        ## records the y velocity data of the ball
                        self.y_vel_array[self.runs] = self.ADC_vel_share[1].read()
                        ## records the roll angular velocity of the platform
                        self.roll_vel_array[self.runs] = self.ang_vel_share[0].read()
                        ## records the pitch angular velocity of the platform
                        self.pitch_vel_array[self.runs] = self.ang_vel_share[1].read()
                        
                        self.runs += 1
                        
                   if self.ser.any():
                       ## Creates a string variable that contains any input data from
                       #  strokes on the keyboard as the letter that is pressed. 
                       char_in = self.ser.read(1).decode()
                       if (char_in == 's' or char_in == 'S'):
                            print('Data Collection Ended Early')
                            self.transition_to(S1_print)
                            
                   elif self.runs >= (10e3/self.period)+1:
                         self.transition_to(S1_print)
                         
                   else: 
                         pass  
                
                elif self.collect_share.read() == False:
                    pass
                
            elif self.state == S1_print:
                if self.count < self.runs:                       
                     ## Creates a time difference array that counts the time in which this iteration of the count has
                     # taken place
                     self.diff_array[self.count] = utime.ticks_diff(self.time_array[self.count], self.time_array[0])/1000
                     print(self.diff_array[self.count], self.ADCx_array[self.count], self.ADCy_array[self.count], self.roll_array[self.count], self.pitch_array[self.count], self.x_vel_array[self.count], self.y_vel_array[self.count], self.roll_vel_array[self.count], self.pitch_vel_array[self.count]) 
                     self.count += 1  
                else:
                     self.ADCx_array = array.array('d', range(201))           
                     self.ADCy_array = array.array('d', range(201))
                     self.x_vel_array = array.array('d', range(201))
                     self.y_vel_array = array.array('d', range(201))
                     self.time_array = array.array('i', range(201))
                     self.roll_array = array.array('d', range(201))
                     self.pitch_array = array.array('d', range(201))
                     self.diff_array = array.array('d',range(201))
                     self.roll_vel_array = array.array('d', range(201))
                     self.pitch_vel_array = array.array('d', range(201))
                     self.print_count = 0
                     self.runs = 0
                     self.collect_share.write(False)
                     self.transition_to(S0_collect)    
                     
            self.next_time = utime.ticks_add(self.next_time, self.period)
        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        self.state = new_state