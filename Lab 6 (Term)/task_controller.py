''' @file           task_controller.py
    @brief A task used to implement a P controller to run a motor
    @details This task when called creates a class object that takes in 7 shares 
             to set and execute the methods in it. From there it writes the value
             of the ADC and IMU angle value and shares it to the controller driver
             where it is calculates the duty cycle of the motor.
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/7/21
    @page page Controller Testing with an encoder *Update 12/4 using the IMU instead of encoder
          Controller was tested using various gains and set speeds.
          Below are various trials conducted at different gains.
          It was found that as the gains increased the motor varied less in speed
          \image html stepresponse1.png "Step Response Using -10 Gain at Target 56 Rad/s"
          \image html stepresponse2.png "Step Response Using -8 Gain at Target 56 Rad/s"
          \image html stepresponse3.png "Step Response Using -15 Gain at Target 56 Rad/s"
          \image html stepresponse4.png "Step Response Using -20 Gain at Target 56 Rad/s"
          
          The controller tested above can be visualized in the block diagram below
          \image html block_diagram.jpg "Controller Block Diagram"
          
          To run the controller various task are running simultaniously.
          Below is an image of the Task Digram used to run the controller
          \image html Task_Diagram.jpg "Task Diagram"
          
          Lastly, the images below are all the state transition diagram for each task
          that implemented a Finite State Machine.
          \image html encoder_fsm.jpg "Task_Encoder Finite State Machine Diagram"
          \image html user_fsm.jpg "Task_Uer Finite State Machine Diagram"
          \image html controller_fsm.jpg "Task_Controller Finite State Machine Diagram"
          \image html motor_fsm.jpg "Task_Motor Finite State Machine Diagram"
          

    
'''
import Driver_Controller
import utime
from ulab import numpy as np

class task_controller():
     ''' @brief     A class used to control the motor output to balance the panel
         @details   This class is a task taking in ADC readings and euler readings
                     from the touch panel and IMU to control the behavior of the motor 
                     to balance the panel.
     '''
     def __init__(self, state_control_share, ADC_share, ADC_vel_share, euler_angle_share, ang_vel_share, motor_share, z_share):
        ''' @brief Initializes variables for the controller task
            @details This method initiates the task controller variables that will read ADC and 
                     euler angle values to direct the motor to the equilibrium point of the system
            @param state_control_share inputs a share that reads which state the ctroller needs to be in           
            @param ADC_share inputs a share that reads the x and y data from the touch panel
            @param ADC_vel_share inputs a share that reads the x velocity and y velocity from 
                   the touch panel
            @param euler_angle_share inputs a share that reads the euler agnles from the IMU
            @param ang_vel_share inputs a share that reads the euler angular velocities from the IMU
            @param motor_share inputs a share that writes the motor duty
            @param z_share inputs a share that reads if the touch panel is being touched or not
        '''    
        ## Initialisation of error variable
        self.error = 0
        
        ## Initializes a share that sets the state in which the controller driver needs to be in
        self.state_control_share = state_control_share
        
        ## Initializes a share that sares the data for the positional data of the object on the panel
        self.ADC_share = ADC_share
        
        ## Initializes a share that shares the data for the positional velcoity of the object on the panel
        self.ADC_vel_share = ADC_vel_share
        
        ## Initializes a share that shares the data for the euler angle of the panel
        self.euler_angle_share = euler_angle_share
    
        ## Initializes a share that shares the data for the angular velocity of the panel
        self.ang_vel_share = ang_vel_share
        
        ## Initializes a share that shares the data for the motor
        self.motor_share = motor_share
        
        ## Initializes a share that shares the data for if an object is on the panel or not
        self.z_share = z_share
       
        ## Initializes a controller driver class object to be used later in the file
        self.controller = Driver_Controller.Driver_Controller()
        
        ## sets the data collection period in which the file will run
        self.period = 5
         
        ## sets when the next time data will be run
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
     def run(self):
         ''' @brief Runs one iteration of the FSM every time it is called
             @details This method runs the task controller FSM 1 time and attempts to balance the panel
         '''
         ## Creates a current time that updates everytime the code is ran. This current
         # time sets the rate at which this program will run
         current_time = utime.ticks_ms()
                 
         if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state_control_share.read() == 1:
                #self.controller.set_Kp(np.array([-5.1725, -1.2801, -0.9681, -0.1201]))
                self.controller.set_Kp(np.array([-6.04, -1.47, -1.19, -0.137]))
                self.state_control_share.write(2)
            elif self.state_control_share.read() == 2:
                if self.z_share.read() == True:
                    self.controller.set_Kp(np.array([-6.5498, -1.5801, -1.3105, -.1462]))
                    self.data = [[self.ADC_share[1].read(), self.euler_angle_share[0].read(), self.ADC_vel_share[1].read(), self.ang_vel_share[0].read()], [self.ADC_share[0].read(), self.euler_angle_share[1].read(), self.ADC_vel_share[0].read(), self.ang_vel_share[1].read()]]
                    print(-self.controller.run(self.data[0]), self.controller.run(self.data[1]))
                    self.motor_share[0].write(self.controller.run(self.data[1]))   #y
                    self.motor_share[1].write(-self.controller.run(self.data[0]))  #x
                else:
                    self.data = [[self.ADC_share[1].read(), self.euler_angle_share[0].read(), self.ADC_vel_share[1].read(), self.ang_vel_share[0].read()], [self.ADC_share[0].read(), self.euler_angle_share[1].read(), self.ADC_vel_share[0].read(), self.ang_vel_share[1].read()]]
                    #print(self.data[0])
                    print(-self.controller.run(self.data[0]), self.controller.run(self.data[1]))
                    self.motor_share[0].write(self.controller.run(self.data[1]))   #y
                    self.motor_share[1].write(-self.controller.run(self.data[0]))  #x
            else: 
                pass
            self.next_time = utime.ticks_add(self.next_time, self.period)
            