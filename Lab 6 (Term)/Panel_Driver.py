''' @file Panel_Driver.py
    @brief A driver for a Touch Panel
    @details This file creates a panel driver class that contains 5 methods that
             can be used in other files to read x, y, z data from the touch panel,
             read and calculate calbration constants and return the last reading from
             all three axiss from the touch panel. It contains a contructor method to
             instantiate each class object. A x scan method to read the ADC value in
             the x direction.  A y scan method to read the ADC value in the y direction.
             A z scan method to read the ADC value in the z direction. A read method to 
             return the last ADC value of all three axis. A calibrate method to take in 
             nine different selected locations on the panel to create calibration 
             coefficients to correct the ADC readings.
    @author Cade Liberty
    @author Chris Suzuki
    @date 12/9/21
'''

import pyb, array
from ulab import numpy as np
class Panel_Driver():
     ''' @brief Interface with touch panel hardware
         @details Creates a class that can be called into other python files that
                 is used to interface and produce a ADC value for the x, y, z coordinate
                 for the touch panel.The class contains 5 methods. One to construct a 
                 panel object, one to scan the x axis, one to scan the y axis, one to 
                 scan the z axis, one to read all three axis values, and one to create
                 calibration coefficents for all three axis.
     '''
     def __init__(self):
        ''' @brief constructs the panel object
            @details Instantiates a panel object that contains 5 different
                     methods that can be used in other python files. 
        '''            
                     
        ## sets pin for the ym data of the touch panel
        self.A0 = pyb.Pin.cpu.A0
        
        ## sets pin for the xm data of the touch panel
        self.A1 = pyb.Pin.cpu.A1
        
        ## sets pin for the yp data of the touch panel
        self.A6 = pyb.Pin.cpu.A6
        
        ## sets pin for the xp data of the touch panel
        self.A7 = pyb.Pin.cpu.A7
        
        ## creates an array for panel data for calibration
        self.data = array.array('f', range(3))
        
        ## creates a matrix for panel data for calibration
        self.rec = np.empty((9, 3))
        
        ## setting the calibration coordinates
        self.x = np.array([[-80, -40], [0, -40], [80, -40], [-80, 0], [0, 0], [80, 0], [-80, 40], [0, 40], [80, 40]])
        
        ## sets a variable to count the number of runs through the system
        self.runs = 1
        
        ## sets a boolean variable to true to start a while loop
        self.loop = True
        
     def x_scan (self):
         ''' @brief returns the ADC value in the x direction of the panel
             @details This method will assign pins to read the ADC value in 
                      the x direction. 
             @return returns the ADC value in the x direction of the panel
         ''' 
        
         self.adc_x = pyb.ADC(self.A0)
         self.p7 = pyb.Pin(self.A7, pyb.Pin.OUT_PP)
         self.p1 = pyb.Pin(self.A1, pyb.Pin.OUT_PP)
         self.p6 = pyb.Pin(self.A6, pyb.Pin.IN)
         self.p7.high()
         self.p1.low()
         self.x_scans = self.adc_x.read()
         return self.x_scans
    
     def y_scan (self):
        ''' @brief returns the ADC value in the y direction of the panel
            @details This method will assign pins to read the ADC value in 
                     the y direction. 
            @return returns the ADC value in the y direction of the panel
        ''' 
        self.adc_y = pyb.ADC(self.A1)
        self.p6 = pyb.Pin(self.A6, pyb.Pin.OUT_PP)
        self.p0 = pyb.Pin(self.A0, pyb.Pin.OUT_PP)
        self.p7 = pyb.Pin(self.A7, pyb.Pin.IN)
        self.p6.high()
        self.p0.low()
        self.y_scans = self.adc_y.read()
        return self.y_scans

     def z_scan (self):
        ''' @brief returns if contact is made in the z direction of the panel
            @details This method will assign pins to read the ADC value in 
                      the z direction. 
             @return returns the True or False if contact is made z direction of the panel
        ''' 
        self.adc_z = pyb.ADC(self.A7)
        self.p6 = pyb.Pin(self.A6, pyb.Pin.OUT_PP)
        self.p1 = pyb.Pin(self.A1, pyb.Pin.OUT_PP)
        self.p0 = pyb.Pin(self.A0, pyb.Pin.IN)
        self.p6.high()
        self.p1.low()
        if self.adc_z.read() < 20:
            self.z_scans = False
            return self.z_scans
        else:
            self.z_scans = True
            return self.z_scans

     def read(self):
         ''' @brief prints the last recorded ADC readings in the x, y, z direction.
             @details This method prints data from the x scan, y scan and z scan. 
         ''' 

         self.reads = (self.x_scans, self.y_scans, self.z_scans)
         if self.z_scans == True:
             print(self.reads)
             self.reads = ()
         else:
             pass
             
     def calibrate(self):
         ''' @brief prodcues calibration ocefficients for the touch panel
             @details Loops nine times to get nine different readings of 
                      predesignated locations on the panel to calculate 
                      calibration coefficients.
             @return returns the calbration coefficients
         ''' 
         
         while(self.loop):
             if self.runs <= 9:
                self.adc_z = pyb.ADC(self.A7)
                self.p6 = pyb.Pin(self.A6, pyb.Pin.OUT_PP)
                self.p1 = pyb.Pin(self.A1, pyb.Pin.OUT_PP)
                self.p0 = pyb.Pin(self.A0, pyb.Pin.IN)
                self.p6.high()
                self.p1.low()
                if self.adc_z.read() > 20:
                     self.z_scan = True
                     self.adc_x = pyb.ADC(self.A0)
                     self.p7 = pyb.Pin(self.A7, pyb.Pin.OUT_PP)
                     self.p1 = pyb.Pin(self.A1, pyb.Pin.OUT_PP)
                     self.p6 = pyb.Pin(self.A6, pyb.Pin.IN)
                     self.p7.high()
                     self.p1.low()
                     self.x_scans = self.adc_x.read()
                     self.data[0] = self.x_scans
                     self.adc_y = pyb.ADC(self.A1)
                     self.p6 = pyb.Pin(self.A6, pyb.Pin.OUT_PP)
                     self.p0 = pyb.Pin(self.A0, pyb.Pin.OUT_PP)
                     self.p7 = pyb.Pin(self.A7, pyb.Pin.IN)
                     self.p6.high()
                     self.p0.low()
                     self.y_scans = self.adc_y.read()
                     self.data[1] = self.y_scans
                     self.data[2] = 1
                     self.rec[self.runs-1] = self.data
                     print('Recorded')
                     while(self.z_scan):
                         self.adc_z = pyb.ADC(self.A7)
                         self.p6 = pyb.Pin(self.A6, pyb.Pin.OUT_PP)
                         self.p1 = pyb.Pin(self.A1, pyb.Pin.OUT_PP)
                         self.p0 = pyb.Pin(self.A0, pyb.Pin.IN)
                         self.p6.high()
                         self.p1.low()
                         if self.adc_z.read() < 10:
                             self.z_scan = False
                             print('Next')
                             self.runs += 1
                         else:
                             pass
                else:
                    pass
             else:
                self.B = np.dot(np.dot(np.linalg.inv(np.dot((self.rec.T), self.rec)),self.rec.T),self.x)
                A = self.B[0,1]
                C = self.B[1,0]
                self.B[0,1] = C
                self.B[1,0] = A
                self.B = self.B.flatten()
                self.B = ','.join(str(element) for element in self.B)
                return self.B
                self.loop = False