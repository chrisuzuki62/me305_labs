''' @file Lab_0.py
    @date Sep 23 2021
    @author Chris Suzuki
'''
#Description:
#The following script calculates the fibonacci number corresponding to the entered integer

def fib (idx):
    if idx < 0:             #if the entered number is less than zero
        return None         #output the word None
    elif idx == 0:          #if the entered number is zero
        return 0            #output the value 0
    elif idx == 1:          #if the entered number is one
        return 1            #output the value 1
    elif idx > 1:           #if the entered number is greater than 1
        f1 = 0              #calculate fibbonaci's number by
        f2 = 1
        for i in range(idx-1):  #loop the following commands for the entered integer minus one
                f3 = f1 + f2    #the addition of value 1 and value 2 equals value 3
                f1 = f2         #value two is now value one
                f2 = f3         #value three is now value two
        return f3               #once the total number of loops are finished output the final value 3
        


if __name__ == '__main__':
    while(1):               #always while the script is running
        user = input('Enter a positive integer (or type \'e\' to exit program): ') #user inputs a value next to the text
        
        try:                        
            if user == 'e':         #if the user types e
                print('Goodbye')    #output 'Goodbye'
                break               #exits script
        except:                     #any exception to the if statement outputs the text right below
            print('A non-integer cannot be accepted')  
        
        try:
            idx = int(user)         #the entered integer equals variable idx
            if idx < 0 :            #if idx is less than zero output the message below
                print('A negative integer cannot be accepted')
            else:                   #else if the entered number is greater or equal to zero output the message below
                print('Fibonacci number at '
                  'index {:} is {:}.'.format(idx, fib(idx)))    #ethe first colon is the entered number and the second colon is the calculation of fibonacci number
  
        except:                     #any excpetions to an entered integer will output the message below
            print('A non-integer cannot be accepted')
           
