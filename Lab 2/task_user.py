''' @file           task_user.py
    @brief A task used to check for and implement specific keyboard commands
    @details This task when called will create a task user object from the task
             user class. this class contains 3 methods to perform different tasks.
             The first method initalizes the task suer object. The next method
             when called prompts the user with the specific commands they can use
             and then transition into waiting for a command before running any code.
             From there dependsing on the key stroke it sends data back to the main
             or sends the user to the third method. In the third method the program
             runs through collecting data for 30 seconds or until a stop command is
             issued. From there is prints the data in a list and then runs back to
             the second method.
             \image html IMG_8178.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
    @copyright license term
'''
import pyb, utime
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)

## State 2 of the user interface task
S2_COLLECT_DATA     = const(2)

class task_user():
     ''' @brief     A task for keyboard inputs to call for encoder data
         @details   This class is a task taking in a user input through 
                    a single keyboard key input and displaying the related 
                    information to the input 
     '''
     def __init__(self):
         ''' @brief         Initializes task_user
             @details       This initiates an User objects and prepare 
                            keyboard serial input and other variables
         '''
         ## A serial port to use for user I/O
         self.ser = pyb.USB_VCP()
        
         ## The state to run on the next iteration of the finite state machine
         self.state = S0_INIT
         
         # creates a list that will contain data for 30 seconds
         self.data = []
         
         ## sets the data collection period or frequency
         self.period = 100
         
         ## sets when the next time data will be collected
         self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
         
#         self.program_period = 10000
         
     def intro(self, enc_pos):
         ''' @brief     Introduce and run the user task.
             @details   Uses a finite state machine to interact with the user
                        inputs.Characters are entered through a serial
                        port that cmmunicating through a USB keyboard.
             @param     enc_pos     Updated data collection from task encoder.
             @return    Flag indicating if the encoder needs to be zeroed.
         '''
         while(True):
             if self.state == S0_INIT:
                 print("Welcome to the Nucleo Hardware!", 
                       "Type z to zero the position of encoder 1.",
                       "Type p for the position of encoder 1.",
                       "Type d for the delta for encoder 1.",
                       "Type g to collect encoder 1 data for 30 sec.",
                       "Type s to end collection prematurely:", sep="\n")
                 
                 self.transition_to(S1_WAIT_FOR_CHAR)
             elif self.state == S1_WAIT_FOR_CHAR:
                    if self.ser.any():
                   
                    ## Creates a string variable that contains any input data from
                    #  strokes on the keyboard as the letter that is pressed.
                        char_in = self.ser.read(1).decode()
                    
                        if (char_in == 'z' or char_in == 'Z'):
                            print('Encoder Position Zeroed')
                            return True
                        elif (char_in == 'p' or char_in == 'P'):
                            print(enc_pos[0])
                            return False
                        elif (char_in == 'd' or char_in == 'D'):
                            print(enc_pos[1])
                            return False
                        elif (char_in == 'g' or char_in == 'G'):
                            print ('Collecting Data')
                            self.transition_to(S2_COLLECT_DATA)
                            
                            ## counts the number of times data is collected
                            self.runs = 0
                            return False
                    else:
                        break
                
             elif self.state == S2_COLLECT_DATA:
                 
                 ## starts a timer when the task enters state 2
                 current_time = utime.ticks_ms()
                 
                 if (utime.ticks_diff(current_time, self.next_time) >= 0):        
                     
                     if self.runs < (31e3/self.period)+1:
                         
                         ## records position data from the encoder
                         self.pos = enc_pos[0]
                         self.data.append(self.pos)
                         self.runs += 1
                         
                     elif self.runs >= (31e3/self.period)+1:
                         for idx in self.data:
                             print(idx)
                         self.data = []
                         self.transition_to(S1_WAIT_FOR_CHAR)
                        
                         return False
                     
                     self.next_time = utime.ticks_add(self.next_time, self.period)
                     
                 if self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if (char_in == 's' or char_in == 'S'):
                            print('Data Collection Ended Early')
                            for idx in self.data:
                                print(idx)
                            self.data = []
                            self.transition_to(S1_WAIT_FOR_CHAR)
                            return False
                 else:
                     break
                
                
     def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        self.state = new_state