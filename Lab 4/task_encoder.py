""" @file task_encoder.py
    @brief A task used to retrieve data from an encoder and set the encoder reading to 0
    @details This file creates a task that is used to operate an encoder and provide
             its data in a usable shared format. The class has 2 methods in it. One
             method is used to instatntiate the task encoder objects and the other
             method is used to run the encoderr and retrieve the data from the encoder
             as well as check if the position of the encoder should be zeroed.
             \image html IMG_8180.jpg "Finite State Machine Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
"""
import encoder, pyb, utime, math

class task_encoder():
    ''' @brief Encoder task for Lab 0x02
        @details Implements a finite state machine that contnuosuly updates 
                 the read position of the encoder and returns all data to the 
                 main function
    '''
    def __init__(self, encoder1_share, encoder2_share, zflag1, zflag2):
        ''' @brief Constructs an Encoder Task
            @details The encoder task implements the finite state machine and
                     links all of the tasks to the specifed encoder
            @param encoder1_share inputs a share that links the data from this file
                   to the user task so they can communicate
            @param encoder2_share inputs a share that links the data from this file
                   to the user task so they can communicate
            @param zflag1 inputs a share that sahres a flag to zero the encoder between
                   the user task and here
            @param zflag1 inputs a share that sahres a flag to zero the encoder between
                   the user task and here
        '''
        
        ## Initalizes the use of an encoder to generate data
        self.encoders1 = encoder.encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
        
        ## Initalizes the use of a second encoder to generate data
        self.encoders2 = encoder.encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8)
        
        ## Initalizes a shares object that contains the encoder data to share
        # the data between task user and this encoder task
        self.encoder1_share = encoder1_share
        
        ## Initalizes a shares object that contains the encoder data to share
        # the data between task user and this encoder task
        self.encoder2_share = encoder2_share
        
        ## Initalizes a shares object that contains the a flag to zero the encoder to share
        # the data between task user and this encoder task
        self.zflag1 = zflag1
        
        ## Initalizes a shares object that contains the a flag to zero the encoder to share
        # the data between task user and this encoder task
        self.zflag2 = zflag2
        
        ## sets the data collection period or frequency
        self.period = 5
         
        ## sets when the next time data will be run
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        self.last_time = 0
        
    def run(self):
        ''' @brief Runs one iteration of the FSM every time it is called
            @details This method runs the encoder FSM machine 1 time and returns
                     the position that the encoder reads of the shaft
        '''
        ## creates a current time that updates everytime the code is ran. This current
        # time sets the rate at which this program will run
        current_time = utime.ticks_ms()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            self.encoders1.update()
            self.encoders2.update()
    
            if self.zflag1.read() == True:
                self.encoders1.set_position(0)
                self.zflag1.write(False)
                
            elif self.zflag2.read() == True:
                self.encoders2.set_position(0)
                self.zflag2.write(False)
                
            else:
                self.encoder1_share[0].write(self.encoders1.get_position()*((2*math.pi)/4000))
                self.encoder1_share[1].write(self.encoders1.get_delta()*((2*math.pi)/4000)*(1/(utime.ticks_diff(current_time, self.last_time)/1000)))
                self.encoder2_share[0].write(self.encoders2.get_position()*((2*math.pi)/4000))
                self.encoder2_share[1].write(self.encoders2.get_delta()*((2*math.pi)/4000)*(1/(utime.ticks_diff(current_time, self.last_time)/1000)))
                self.last_time = current_time
            self.next_time = utime.ticks_add(self.next_time, self.period)
            