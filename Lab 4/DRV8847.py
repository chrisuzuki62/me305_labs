''' @file DRV8847.py
''' 
import pyb, utime
class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV88477
        motor driver and to create one or more objects of the
        Motor class which can be used to perform motor9control.
        
        Refer to the DRV8847 datasheet here:
        https://www.ti.com/lit/ds/symlink/drv8847.pdf13
    '''
    
    def __init__ (self):
       ''' @brief      Initializes and returns a DRV8847 object.17
           @details Creates a class that is used to interface with a motor and
                    set its duty cycle. The class has 4 methods. One to construct
                    the motor object, one to enable to motors to run, one to disable
                    the motors from running, one to fault the motors if there is a 
                    fault detected and one to create a motor object.
       '''  
       ## Creates a sleep pin that can be toggled high to turn on the motors and 
       #  low to turn off the motors
       self.nSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
       
       ## Creates a fault pin to allow the code to read for any faults
       fault = pyb.Pin(pyb.Pin.cpu.B2)
       
       ## Creates a fault callback that is run whenever a fault is detected
       FaultInt = pyb.ExtInt(fault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
       
       ## Creates a variable that enables the fault callback from to be enabled or
       # disabled
       self.fault_trig = FaultInt
       pass
     
    def enable (self):
       ''' @brief      Brings the DRV8847 out of sleep mode.
           @ details This method when ran enables the motors to be run by setting the
                     sleep pin to high. It also gets circumvents any false early fault
                     flags that prevent the system from running.
       '''
       self.fault_trig.disable()                # Disable fault interrupt
       self.nSleep.high()                       # Re-enable the motor driver
       utime.sleep_us(25)                       # Wait for the fault pin to return high
       self.fault_trig.enable()                 # Re-enable the fault interrupt
       print('Motors Enabled')
       pass
      
    def disable (self):
       ''' @brief      Puts the DRV8847 in sleep mode.
           @details This method when ran will disable the motors by setting the sleep
                    pin to low.
       '''
       self.nSleep.low()
       print('Motors Disabled')
       pass 
    
    def fault_cb (self, IRQ_src):
       ''' @brief      Callback function to run on fault condition.
           @details This method is run anytime there is a fault detected and shuts
                    down the motors to protect them from damage. It does this by
                    setting the sleep pin to low.
           @param      IRQ_src The source of the interrupt request.
       ''' 
       self.nSleep.low()
       print('Fault Detected')
       pass  
   
    def motor (self, A, PinA, B, PinB):
       ''' @brief      Initializes and returns a motor object associated with the DRV8847.
           @details This method when called sets the values for the motor that will be
                    called allowing for the motor class to be run.
           @param A This is the channel to the timer that is connected to the first motor
                  and enables the motor to run
           @param Pin A This is the pin that the channel A is connected to
           @param B This is the channel to the timer that is connected to the first motor
                  and enables the motor to run
           @param Pin B This is the pin that the channel A is connected to
           @return     An object of class Motor39
       '''         
       return Motor(A, PinA, B, PinB)
   
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given 45DC motor.
    '''      
    def __init__ (self, A, PinA, B, PinB):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                           directly. Instead create a DRV8847 object and use
                           that to create Motor objects using the method53DRV8847.motor().
        ''' 
        ## Defines the frequency that the motor will run at
        self.freq = 25000 
        ## Defines the timer that the encoder will use
        self.timX = pyb.Timer(3, freq = self.freq)
        ## Sets a channel of the time that will be used to control the motor
        self.tXchA = self.timX.channel(A, pyb.Timer.PWM, pin=PinA)
        ## Sets a channel of the time that will be used to control the motor
        self.tXchB = self.timX.channel(B, pyb.Timer.PWM, pin=PinB) 
        pass      
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor65
         '''          
        if duty > 0:
            self.tXchA.pulse_width_percent(duty)
            self.tXchB.pulse_width_percent(0)
        elif duty < 0:
            self.tXchB.pulse_width_percent(abs(duty))
            self.tXchA.pulse_width_percent(0)
        elif duty == 0:
            self.tXchB.pulse_width_percent(duty)
            self.tXchA.pulse_width_percent(duty)
        pass  