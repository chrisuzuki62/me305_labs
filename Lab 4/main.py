''' @file           main.py
    @brief Runs the main program and creates all objects to run the encoder
    @details This program starts up on a reboot and runs through. It first creates
             all of the class objects from the task user and task encoder classes
             and then uses them in a loop fashion to retrieve data and share data
             between them both. Finally to exit the user must enter a keyboard
             interupt.
             \image html IMG_8179.jpg "Task and Shares Diagram"
    @author Cade Liberty
    @author Chris Suzuki
    @date 10/21/21
    @copyright license term
'''

import task_user
import task_encoder
import task_motor
import task_controller
#import pyb
import shares

#Running the code
if __name__ =='__main__':
    
    ## Creates an share object that contains two shares
    encoder1_share = (shares.Share(0), shares.Share(0))
    ## Creates a share object that is used to set the encoder to zero
    zflag1 = shares.Share(False)
    ## Creates a share object that is used to set the encoder to zero
    zflag2 = shares.Share(False)
    ## Creates a share object that is used to set a motor to run
    motor1_share = shares.Share(0)
    ## Creates an share object that contains two shares
    encoder2_share = (shares.Share(0), shares.Share(0))
    ## Creates a share object that is used to set a motor to run
    motor2_share = shares.Share(0)
    ## Creates a share object that is used to clear a fault when it occurs
    fault_share = shares.Share(False)
    ## Creates a share object that is used to set a the velocity setpoint
    vel_share = shares.Share(0)
    ## Creates a share object that is used to set a gain for the controller
    gain_share = shares.Share(1)
    ## Creates a share object that is used to set the state of the controller
    state_control_share = shares.Share(0)
    ## Creates a share object that is used to set which motor to control
    motor_control_share = shares.Share(0)
    
    
    ## Initalizes a an object that is able to use the methods defined in the task
    #  user class
    task1 = task_user.task_user(motor1_share, encoder1_share, motor2_share, encoder2_share, zflag1, zflag2, fault_share, vel_share, gain_share, state_control_share, motor_control_share)
    
    ## Initalizes a an object that is able to use the methods defined in the task
    #  encoder class
    task0 = task_encoder.task_encoder(encoder1_share, encoder2_share, zflag1, zflag2)
    
    ## Initalizes a an object that is able to use the methods defined in the task motor class
    task2 = task_motor.task_motor(motor1_share, motor2_share, fault_share)
    
    ## Initalizes a an object that is able to use the methods defined in the task controller class
    task3 = task_controller.task_controller(vel_share, encoder1_share, encoder2_share, gain_share, state_control_share, motor_control_share, motor1_share, motor2_share)
    
    
    while(True):
        try:
            task0.run()
            task1.intro()
            task3.run()
            task2.run()
                    
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
